En estos momentos me enceuntro aprendiendo e enriqueciendome de conocimiento sobre todo lo referente con el administrador de wagtail, ya que me estoy desempeñando en el area de Desarrollo Frontend.

¿Que es Fronted?

Front End es la parte de una aplicación que interactúa con los usuarios, es conocida como el lado del cliente. Básicamente es todo lo que vemos en la pantalla cuando accedemos a un sitio web o aplicación: tipos de letra, colores, adaptación para distintas pantallas(RWD), los efectos del ratón, teclado, movimientos, desplazamientos, efectos visuales… y otros elementos que permiten navegar dentro de una página web. Este conjunto crea la experiencia del usuario.
El desarrollador front end se encarga de la experiencia del usuario, es decir,  en el momento en el que este entra a una página web, debe ser capaz de navegar por ella, por lo que el usuario verá una interface sencilla de usar, atractiva y funcional.

¿Que es Wagtail?

Wagtail es un CMS de código abierto escrito en Python y construido en el marco Django. Está desarrollado por desarrolladores para desarrolladores, y proporciona una interfaz rápida y atractiva para los editores, lo que permite a los editores crear y estructurar contenido de forma intuitiva. Es elegante, potente y ágil.Echemos un vistazo al efecto del sitio web logrado por wagtail.

