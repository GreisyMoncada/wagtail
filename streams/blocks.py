#Streamfields live in here.

from wagtail.core import blocks 
from wagtail.images.blocks import ImageChooserBlock



class TitleAndTextBlock(blocks.StructBlock):
    #Title and text and nothing else.

    title = blocks.CharBlock(required=True, help_text='Add your title')
    text = blocks.TextBlock(required=True, help_text='Add additional text')

    class Meta: #noga
        template = "streams/title_and_text_block.html"
        icon = "edit"
        label = "Title & Text"

class CardBlock(blocks.StructBlock):
    # Cards with image and text and button(s).

    title = blocks.CharBlock(required=True, help_text='Add your title')

    cards = blocks.ListBlock(
        blocks.StructBlock (
            [
                ("image", ImageChooserBlock(Required=True)),
                ("title", blocks.CharBlock(required=True, max_length=40)),
                ("text", blocks.TextBlock(required=True, max_length=200)),
                ("button_page", blocks.PageChooserBlock(required=False)),
                ("button_url", blocks.URLBlock(required=False, help_text="if the button page is selected, what be used first.")),
            ]
        )
    )
    class Meta: #noga
        template = "streams/card_block.html"
        icon = " placeholder "
        label = " Staff Cards "


class RichTextBlock(blocks.RichTextBlock):
    #RichTex with all the features.

    class Meta: #noga
        template = "streams/richtext_block.html"
        icon = "doc-full"
        label = " Full RichText "

class SimpleRichTextBlock(blocks.RichTextBlock):
    #RichTex without (limited) all the features.

    def __init__(self, required=True, help_text=None, editor='default', features=None, validators=(), **kwargs):
        super().__init__(**kwargs)
        self.features = ["bold","italic","link",]
    class Meta: #noga
        template = "streams/richtext_block.html"
        icon = "doc-full"
        label = "Simple RichText"


class CTABlock(blocks.StructBlock):
    """A simple call to action section."""

    title = blocks.CharBlock(required=True, max_length=60)
    text = blocks.RichTextBlock(required=True, features=["bold","italic"])
    Button_page = blocks.PageChooserBlock(required=False)
    Button_url = blocks.URLBlock(required=False)
    Button_text = blocks.CharBlock(required=True, default='Learn More', max_length=40)

    class Meta:
        template = "streams/cta_block.html"
        icon = "placeholder"
        label = " Call to Action"

